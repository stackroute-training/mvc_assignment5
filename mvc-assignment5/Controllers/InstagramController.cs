﻿using Microsoft.AspNetCore.Mvc;
using mvc_assignment5.Models;
using mvc_assignment5.Services;
using System.Collections.Generic;

namespace mvc_assignment5.Controllers
{
    public class InstagramController : Controller
    {
        private readonly InstagramService instagramService;
        public InstagramController()
        {
            this.instagramService = new InstagramService();
        }
        public IActionResult Index()
        {
            List<InstagramModel> insta = new List<InstagramModel>();
            List<Instagram> instagram = instagramService.GetAll();
            foreach (Instagram instag in instagram)
            {
                insta.Add(new InstagramModel() { Id = instag.Id,Username = instag.Username,Fullname = instag.Fullname, Email = instag.Email, Comments = instag.Comments, Instaid = instag.Instaid, Likes = instag.Likes, Posts = instag.Posts});
            }
            return View(insta);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(InstagramModel instagramModel)
        {
            if (ModelState.IsValid)
            {
                Instagram instagram = new Instagram()
                {
                    Id = instagramModel.Id,
                    Username = instagramModel.Username,
                    Fullname = instagramModel.Fullname,
                    Email = instagramModel.Email,
                    Comments = instagramModel.Comments,
                    Instaid = instagramModel.Instaid,
                    Likes = instagramModel.Likes,
                    Posts = instagramModel.Posts
                };
                instagramService.AddInsta(instagram);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }

        }

        public IActionResult Details(int id)
        {
            Instagram instagram = instagramService.GetById(id);
            InstagramModel model = new InstagramModel() { Comments = instagram.Comments, Email = instagram.Email, Fullname = instagram.Fullname, Id = instagram.Id, Instaid = instagram.Instaid, Username = instagram.Username, Likes = instagram.Likes, Posts = instagram.Posts };
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            instagramService.Del(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Instagram instagram = instagramService.GetById(id);
            InstagramModel model = new InstagramModel() { Comments = instagram.Comments, Email = instagram.Email, Fullname = instagram.Fullname, Id = instagram.Id, Instaid = instagram.Instaid, Username = instagram.Username, Likes = instagram.Likes, Posts = instagram.Posts };
            return View(model);
        }
        [HttpPost]
        public IActionResult Edit(InstagramModel instagramModel)
        {
            if (ModelState.IsValid)
            {
                Instagram instagram = new Instagram()
                {
                    Username = instagramModel.Username,
                    Fullname = instagramModel.Fullname,
                    Email = instagramModel.Email,
                    Comments = instagramModel.Comments,
                    Instaid = instagramModel.Instaid,
                    Likes = instagramModel.Likes,
                    Posts = instagramModel.Posts
                };
                this.instagramService.Upd(instagram);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
    }
}
