﻿using Microsoft.AspNetCore.Mvc;
using mvc_assignment5.Models;
using mvc_assignment5.Services;
using System.Collections.Generic;

namespace mvc_assignment5.Controllers
{
    public class FacebookController : Controller
    {
        private readonly FacebookService facebookService;

        public FacebookController()
        {
            this.facebookService = new FacebookService();
        }
        public IActionResult Index()
        {
            List<FacebookModel> fb = new List<FacebookModel>();
            List<Facebook> fb2 = facebookService.GetAll();
            foreach(Facebook fbk in fb2)
            {
                fb.Add(new FacebookModel() { Id = fbk.Id, Username = fbk.Username, Fullname = fbk.Fullname, Email = fbk.Email, Comments = fbk.Comments, Likes = fbk.Likes, Posts = fbk.Posts });
            }
            return View(fb);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        //[Route("facebook/add")]
        public IActionResult Create(FacebookModel facebookmodel)
        {
            if (ModelState.IsValid)
            {
                Facebook facebook = new Facebook()
                {
                    Id = facebookmodel.Id,
                    Username = facebookmodel.Username,
                    Fullname = facebookmodel.Fullname,
                    Email = facebookmodel.Email,
                    Posts = facebookmodel.Posts,
                    Likes = facebookmodel.Likes,
                    Comments = facebookmodel.Comments,
                };
                this.facebookService.AddFB(facebook);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public IActionResult GetById(int id)
        {
            Facebook facebook = facebookService.GetbyID(id);
            FacebookModel facebook1 = new() { 
                Comments = facebook.Comments, 
                Email = facebook.Email, 
                Fullname = facebook.Fullname, 
                Id = facebook.Id, 
                Likes = facebook.Likes, 
                Posts = facebook.Posts, 
                Username = facebook.Username };
            return View(facebook1);
        }

        public IActionResult Delete(int id)
        {
            facebookService.DeleteFB(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Facebook facebook = facebookService.GetbyID(id);
            FacebookModel facebook1 = new()
            {
                Comments = facebook.Comments,
                Email = facebook.Email,
                Fullname = facebook.Fullname,
                Id = facebook.Id,
                Likes = facebook.Likes,
                Posts = facebook.Posts,
                Username = facebook.Username
            };
            return View(facebook1);
        }
        [HttpPost]
        public IActionResult Edit(FacebookModel facebookmodel)
        {
            if (ModelState.IsValid)
            {
                Facebook facebook = new Facebook()
                {
                    Id = facebookmodel.Id,
                    Username = facebookmodel.Username,
                    Fullname = facebookmodel.Fullname,
                    Email = facebookmodel.Email,
                    Posts = facebookmodel.Posts,
                    Likes = facebookmodel.Likes,
                    Comments = facebookmodel.Comments,
                };
                this.facebookService.EditFB(facebook);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
    }
}
