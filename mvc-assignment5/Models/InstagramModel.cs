﻿using System.ComponentModel.DataAnnotations;
namespace mvc_assignment5.Models
{
    public class InstagramModel
    {
        [Required(ErrorMessage ="Please Enter")]
        public int Instaid { get; set; }
        //[Required(ErrorMessage = "Please Enter")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please Enter")]
        public string Fullname { get; set; }
        [Required(ErrorMessage = "Please Enter")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter")]
        public int? Posts { get; set; }
        [Required(ErrorMessage = "Please Enter")]
        public int? Likes { get; set; }
        [Required(ErrorMessage = "Please Enter")]
        public int? Comments { get; set; }
    }
}
