﻿using System;
using System.Collections.Generic;

#nullable disable

namespace mvc_assignment5.Models
{
    public partial class Instagram
    {
        public int Instaid { get; set; }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public int? Posts { get; set; }
        public int? Likes { get; set; }
        public int? Comments { get; set; }

        public virtual Facebook IdNavigation { get; set; }
    }
}
