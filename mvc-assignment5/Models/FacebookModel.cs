﻿using System.ComponentModel.DataAnnotations;
namespace mvc_assignment5.Models
{
    public class FacebookModel
    {
        [Required(ErrorMessage ="Please enter")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please enter")]
        public string Fullname { get; set; }
        [Required(ErrorMessage = "Please enter")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter")]
        public int? Posts { get; set; }
        [Required(ErrorMessage = "Please enter")]
        public int? Likes { get; set; }
        [Required(ErrorMessage = "Please enter")]
        public int? Comments { get; set; }

    }
}
