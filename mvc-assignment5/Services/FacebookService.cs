﻿using mvc_assignment5.Models;
using System.Collections.Generic;
using System.Linq;

namespace mvc_assignment5.Services
{
    public class FacebookService
    {
        private readonly SocialMediaContext context;

        public FacebookService()
        {
            this.context = new SocialMediaContext();
        }

        public List<Facebook> GetAll()
        {
            return this.context.Facebooks.ToList();
        }

        public Facebook GetbyID(int id)
        {
            return this.context.Facebooks.Find(id);
        }

        public void AddFB(Facebook facebook)
        {
            context.Facebooks.Add(facebook);
            context.SaveChanges();
        }

        public void DeleteFB(int id)
        {
            Facebook facebook = this.GetbyID(id);
            this.context.Facebooks.Remove(facebook);
            context.SaveChanges();
        }

        public void EditFB(Facebook facebook)
        {
            this.context.Facebooks.Update(facebook);
            context.SaveChanges();
        }
    }
}
