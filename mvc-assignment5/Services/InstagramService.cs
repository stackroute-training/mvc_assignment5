﻿using mvc_assignment5.Models;
using System.Collections.Generic;
using System.Linq;

namespace mvc_assignment5.Services
{
    public class InstagramService
    {
        private readonly SocialMediaContext context;
        public InstagramService()
        {
            this.context = new SocialMediaContext();
        }

        public void AddInsta(Instagram instagram)
        {
            this.context.Instagrams.Add(instagram);
            context.SaveChanges();
        }

        public List<Instagram> GetAll()
        {
            return this.context.Instagrams.ToList();
        }

        public Instagram GetById(int id)
        {
            return this.context.Instagrams.Find(id);
        }

        public void Del(int id)
        {
            Instagram instagram =  this.context.Instagrams.Find(id);
            context.Instagrams.Remove(instagram);
            context.SaveChanges();
        }

        public void Upd(Instagram instagram)
        {
            this.context.Instagrams.Update(instagram);
            context.SaveChanges();
        }
    }
}
